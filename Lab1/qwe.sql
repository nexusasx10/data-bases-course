USE master
GO


IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'VadimKisDB'
)
ALTER DATABASE VadimKisDB set single_user with rollback immediate
GO


IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'VadimKisDB'
)
DROP DATABASE VadimKisDB
GO


CREATE DATABASE VadimKisDB
GO


USE VadimKisDB
GO


CREATE TABLE Measures
(
	ID tinyint identity(1, 1) NOT NULL, 
	Name nvarchar(50) NOT NULL, 
	Units nvarchar(15) NOT NULL,
    CONSTRAINT PK_MeasureID PRIMARY KEY (ID) 
)
GO


CREATE TABLE Stations
(
	ID tinyint identity(1, 1) NOT NULL, 
	Name nvarchar(50) NOT NULL, 
	Location nvarchar(50) NOT NULL,
    CONSTRAINT PK_StationID PRIMARY KEY (ID) 
)
GO


CREATE TABLE MeasureEntries
(
	
	Date_ date NOT NULL,
	Time_ time NOT NULL,
	StationID tinyint NOT NULL,
	MeasureID tinyint NOT NULL, 
	Value numeric(10,5) NOT NULL,

)
GO


ALTER TABLE MeasureEntries ADD
	CONSTRAINT FK_StationID FOREIGN KEY (StationID)
	REFERENCES Stations (ID)
GO


ALTER TABLE MeasureEntries ADD
	CONSTRAINT FK_MeasureID FOREIGN KEY (MeasureID)
	REFERENCES Measures (ID)
GO


INSERT INTO Measures
VALUES
(N'Temperature', N'Celsius'),
(N'Wind Speed', N'm/s'),
(N'Rainfall Level', N'mm'),
(N'Humidity', N'percent')
GO


INSERT INTO Stations
VALUES
(N'Vostok', N'Moskow'),
(N'Mir', N'Ekatherinburg'),
(N'Lol', N'Kiev'),
(N'Miracle', N'New York'),
(N'Qwerty', N'Rostov-Na-Donu')
GO


INSERT INTO MeasureEntries
VALUES
('2000-01-02', '01:01:01', 1, 2, 12),
('2000-01-03', '02:02:02', 1, 3, 0),
('2000-01-04', '03:03:03', 1, 4, 70),
('2000-01-01', '04:04:04', 2, 1, 28),
('2000-01-02', '05:05:05', 2, 2, 13),
('2000-01-03', '00:00:00', 2, 3, 2),
('2000-01-04', '01:01:01', 2, 4, 80),
('2000-01-01', '02:02:02', 3, 1, 16),
('2000-01-02', '03:03:03', 3, 2, 8),
('2000-01-03', '04:04:04', 3, 3, 6),
('2000-01-04', '05:05:05', 3, 4, 78),
('2000-01-01', '00:00:00', 4, 1, 12),
('2000-01-02', '01:01:01', 4, 2, 2),
('2000-01-03', '02:02:02', 4, 3, 1),
('2000-01-04', '03:03:03', 4, 4, 87),
('2000-01-01', '04:04:04', 5, 1, 12),
('2000-01-02', '05:05:05', 5, 2, 17),
('2000-01-03', '00:00:00', 5, 3, 3),
('2000-01-04', '01:01:01', 5, 4, 71)
GO


/*SELECT DATENAME(day, MeasureEntries.Date_) + N' '
		+ DATENAME(month, MeasureEntries.Date_) + N' '
		+ DATENAME(year, MeasureEntries.Date_) as ����,
		Stations.Name AS �������, Measures.Name as ��������,
		CAST(ROUND(AVG(MeasureEntries.Value), 1) AS numeric(10,1)) as ��������
	FROM MeasureEntries, Measures, Stations
		WHERE Measures.ID = MeasureEntries.MeasureID
		AND Stations.ID = MeasureEntries.StationID
	GROUP BY  MeasureEntries.Date_, Stations.Name, Measures.Name
GO
*/
/*
SELECT	Stations.Name AS �������,
		Measures.Name as ��������,
		CAST(ROUND(AVG(A.Value), 1) AS numeric(10,1)) as ��������
	FROM MeasureEntries A CROSS JOIN MeasureEntries B, Measures, Stations
		WHERE (Measures.ID = A.MeasureID
		OR Measures.ID = B.MeasureID)
		AND (Stations.ID = A.StationID
		OR Stations.ID = B.StationID)
	GROUP BY Measures.Name, Stations.Name
GO*/

SELECT Measures.Name as ��������,
		CAST(ROUND(AVG(A.Value), 1) AS numeric(10,1)) as ��������
	FROM MeasureEntries A CROSS JOIN MeasureEntries B, Measures
		WHERE (Measures.ID = A.MeasureID OR Measures.ID = B.MeasureID)
	GROUP BY Measures.Name
GO