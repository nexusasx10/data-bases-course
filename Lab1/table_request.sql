USE master
GO

IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'Sheludyakov2'
)
ALTER DATABASE Sheludyakov2 set single_user with rollback immediate
GO

IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'Sheludyakov2'
)
DROP DATABASE Sheludyakov2
GO

CREATE DATABASE Sheludyakov2
GO

USE Sheludyakov2
GO

IF EXISTS(
  SELECT *
    FROM sys.schemas
   WHERE name = N'Shell'
) 
DROP SCHEMA Shell
GO

CREATE SCHEMA Shell 
GO

CREATE TABLE Shell.StationInfo
(
	StationID smallint IDENTITY(0,1) NOT NULL, 
	StationName nvarchar(64) NULL, 
	StationLocation nvarchar(64) NULL, 
    CONSTRAINT PK_StationID PRIMARY KEY (StationID) 
)
GO

CREATE TABLE Shell.MeasureInfo
(
	MeasureID smallint IDENTITY(0,1) NOT NULL, 
	MeasureType nvarchar(64) NULL, 
	MeasureUnit nvarchar(64) NULL,
    CONSTRAINT PK_MeasureID PRIMARY KEY (MeasureID)
)
GO

CREATE TABLE Shell.ObservationInfo
(
	ObservationDate date NULL,
	MeasureID smallint NULL,
	StationID smallint NULL,
	ObservationValue real NULL
)
GO

INSERT INTO Shell.StationInfo
VALUES
(N'����������-360', N'������������, ���������� ���������'),
(N'�������-1995', N'������, ���������� ���������'),
(N'��-12', N'��������, ������')
GO

INSERT INTO Shell.MeasureInfo
VALUES
(N'�����������', N'������ �� �������'),
(N'�����������', N'������ �� ����������'),
(N'�������� �����', N'���� � �������'),
(N'��������', N'�������'),
(N'��������', N'��������� �������� ������')
GO

INSERT INTO Shell.ObservationInfo
VALUES
('2016-12-30', 0, 0, -10),
('2016-12-30', 0, 0, -8),
('2016-12-30', 0, 1, -22.4),
('2016-12-30', 1, 2, -1),
('2016-12-30', 2, 0, 2),
('2016-12-30', 2, 1, 3),
('2016-12-30', 2, 1, 3.1),
('2016-12-30', 2, 1, 3),
('2016-12-30', 2, 2, 5.2),
('2016-12-30', 2, 2, 5),
('2016-12-30', 2, 2, 6),
('2016-12-30', 2, 2, 6.2),

('2016-12-31', 0, 0, -12),
('2016-12-31', 0, 1, -25),
('2016-12-31', 1, 2, 4),
('2016-12-31', 2, 0, 1),
('2016-12-31', 2, 0, 1.1),
('2016-12-31', 2, 0, 1.1),
('2016-12-31', 2, 0, 1),
('2016-12-31', 2, 1, 1),
('2016-12-31', 2, 1, 1.1),
('2016-12-31', 2, 1, 1),
('2016-12-31', 2, 2, 5),

('2017-01-01', 0, 0, -10),
('2017-01-01', 0, 1, -12.3),
('2017-01-01', 1, 1, -10.2),
('2017-01-01', 1, 2, 5),
('2017-01-01', 2, 0, 3),
('2017-01-01', 2, 1, 4.5),
('2017-01-01', 2, 2, 3),
('2017-01-01', 2, 2, 5),
('2017-01-01', 2, 2, 5.2),

('2017-01-02', 0, 0, -10.1),
('2017-01-02', 0, 1, -12),
('2017-01-02', 0, 1, -11),
('2017-01-02', 0, 1, -11.1),
('2017-01-02', 1, 2, 0),
('2017-01-02', 1, 2, -1),
('2017-01-02', 1, 2, -0.1),
('2017-01-02', 2, 0, 3),
('2017-01-02', 2, 1, 4),
('2017-01-02', 2, 1, 5),
('2017-01-02', 2, 2, 5),
('2017-01-02', 2, 2, 3.5)
GO

ALTER TABLE Shell.ObservationInfo ADD 
	CONSTRAINT FK_StationID FOREIGN KEY (StationID) 
	REFERENCES Shell.StationInfo(StationID)
GO

ALTER TABLE Shell.ObservationInfo ADD 
	CONSTRAINT FK_MeasureID FOREIGN KEY (MeasureID) 
	REFERENCES Shell.MeasureInfo(MeasureID)
GO

SELECT DATENAME(dd, ObservationDate) + ' '
 + DATENAME(month, ObservationDate) + ' '
 + DATENAME(yy, ObservationDate) AS ����, 
StationName AS �������, 
MeasureType AS [��� ���������],
MeasureUnit AS [������� ���������],
ROUND(AVG(ObservationValue), 1) AS [������� ��������]
FROM Shell.ObservationInfo 
INNER JOIN Shell.StationInfo
ON Shell.ObservationInfo.StationID = Shell.StationInfo.StationID
INNER JOIN Shell.MeasureInfo
ON Shell.ObservationInfo.MeasureID = Shell.MeasureInfo.MeasureID
GROUP BY ObservationDate, StationName, MeasureUnit, MeasureType
GO
