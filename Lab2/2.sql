USE master
GO


IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'VadimKis'
)
    ALTER DATABASE VadimKis set single_user with rollback immediate
GO

IF  EXISTS (
	SELECT name 
		FROM sys.databases 
		WHERE name = N'VadimKis'
)
	DROP DATABASE VadimKis
    CREATE DATABASE VadimKis
GO


 USE VadimKis
 GO


CREATE TABLE RegionNames
(
	Code tinyint NOT NULL,
	Name nvarchar(30) NOT NULL,
	CONSTRAINT PK_RegionCode PRIMARY KEY (Code)
)
GO


CREATE TABLE RegionNumbers
(
	Number smallint NOT NULL CHECK ((0 <= Number AND Number < 300) OR (700 <= Number AND Number < 800)),
	Code tinyint NOT NULL,
	CONSTRAINT PK_Number PRIMARY KEY (Number),
	CONSTRAINT FK_RegionCode FOREIGN KEY (Code)
	REFERENCES RegionNames (Code)
)
GO


CREATE TABLE Posts
(
	ID tinyint identity(1, 1) NOT NULL,
	Location nvarchar(20) NOT NULL,
	CONSTRAINT PK_PostID PRIMARY KEY (ID)
)
GO


CREATE TABLE Passes
(
	PostID tinyint NOT NULL,
	CarNumber nchar(6) NOT NULL CHECK (CarNumber LIKE '[АВЕКМНОРСТУХ][0-9][0-9][0-9][АВЕКМНОРСТУХ][АВЕКМНОРСТУХ]'),
	CarRegionNumber smallint NOT NULL,
	PassTime time NOT NULL,
	Direction nvarchar(3) NOT NULL,
	CONSTRAINT FK_PostID FOREIGN KEY (PostID)
	REFERENCES Posts (ID),
	CONSTRAINT FK_CarRegionNumber FOREIGN KEY (CarRegionNumber)
	REFERENCES RegionNumbers (Number)
)
GO


CREATE TRIGGER insertRegionCode ON Passes FOR INSERT
AS
declare @l nchar(6);
BEGIN
	set @l = (SELECT TOP 1 Passes.CarNumber
		FROM Passes, inserted
		WHERE Passes.CarNumber = inserted.CarNumber
			AND Passes.CarRegionNumber = inserted.CarRegionNumber
			AND Passes.PassTime < inserted.PassTime
			AND inserted.Direction = Passes.Direction
		ORDER BY Passes.PassTime DESC
	)
	if (len(@l) > 0)
	BEGIN
		PRINT 'Недопустимая запись ' + @l;
		ROLLBACK TRANSACTION;
	END
END
GO


INSERT INTO RegionNames
VALUES
(1, N'Респ. Адыгея'),
(24, N'Красноярский край'),
(38, N'Иркутская обл.'),
(50, N'Московская обл.'),
(66, N'Свердловская обл.'),
(69, N'Тверская обл.'),
(35, N'Вологодская обл.'),
(42, N'Кемеровская обл.'),
(56, N'Оренбургская обл.'),
(4, N'Респ. Алтай'),
(45, N'Курганская обл.')
GO


INSERT INTO RegionNumbers
VALUES
(01, 1),
(24, 24),
(84, 24),
(88, 24),
(124, 24),
(38, 38),
(85, 38),
(138, 38),
(50, 50),
(90, 50),
(150, 50),
(190, 50),
(750, 50),
(66, 66),
(96, 66),
(196, 66),
(69, 69),
(35, 35),
(42, 42),
(142, 42),
(56, 56),
(04, 4),
(45, 45)
GO


INSERT INTO Posts
VALUES
(N'North'),
(N'South'),
(N'West'),
(N'East'),
(N'Heaven'),
(N'Hell')
GO


INSERT INTO Passes
VALUES
(2, N'А218КВ', 196, '00:08:00', N'out'),
(4, N'А218КВ', 196, '16:12:02', N'in'),
(6, N'У858ММ', 66, '00:57:58', N'out'),
(6, N'У858ММ', 66, '14:15:08', N'in'),
(3, N'У825ХК', 150, '01:05:23', N'out'),
(3, N'У825ХК', 150, '15:45:43', N'in'),


(1, N'В373ТА', 190, '01:16:25', N'in'),
(2, N'В373ТА', 190, '04:10:12', N'out'),
(4, N'Т206УН', 38, '20:17:52', N'in'),
(5, N'Т206УН', 38, '21:25:47', N'out'),


(2, N'А121АЕ', 750, '11:09:00', N'in'),
(2, N'А121АЕ', 750, '12:15:20', N'out'),
(3, N'У825ХВ', 150, '01:05:23', N'in'),
(3, N'У825ХВ', 150, '13:05:23', N'out'),


(2, N'А111АА', 66, '10:50:00', N'in'),
(6, N'А647СК', 50, '20:39:11', N'out'),
(2, N'А679ВУ', 750, '14:42:34', N'out'),
(1, N'В540РЕ', 150, '10:33:21', N'out'),
(4, N'К773ОМ', 69, '12:56:58', N'in'),
(4, N'Р476ВР', 138, '06:23:15', N'out'),
(6, N'Р644АР', 96, '07:39:17', N'out'),
(3, N'У314МЕ', 35, '20:05:46', N'out'),
(4, N'Х923ОУ', 42, '11:09:05', N'in')
GO


-- Показать местные автомобили для региона Х
SELECT a.CarNumber + convert(nvarchar(3), a.CarRegionNumber) as [Номер и код региона], RegionNames.Name as [Регион]
FROM Passes a, RegionNames, RegionNumbers
WHERE a.CarRegionNumber = RegionNumbers.Number
	AND RegionNumbers.Code = RegionNames.Code
	AND RegionNames.Name = N'Свердловская обл.'
	AND a.Direction = N'out'
	AND EXISTS(
		SELECT TOP 1 * FROM Passes b
		WHERE a.PassTime < b.PassTime
			AND a.CarNumber = b.CarNumber
			AND a.CarRegionNumber = b.CarRegionNumber
			AND b.Direction = N'in'
	)
GROUP BY a.CarNumber, a.CarRegionNumber, RegionNames.Name
GO

-- Показать транзитные автомобили
SELECT a.CarNumber + convert(nvarchar(3), a.CarRegionNumber) as [Номер и код региона], RegionNames.Name as [Регион]
FROM Passes a, RegionNames, RegionNumbers
WHERE RegionNames.Name != N'Свердловская обл.'
	AND RegionNumbers.Code = RegionNames.Code
	AND a.CarRegionNumber = RegionNumbers.Number
	AND a.Direction = N'in'
	AND EXISTS(
		SELECT TOP 1 * FROM Passes b
		WHERE a.CarNumber = b.CarNumber
			AND a.CarRegionNumber = b.CarRegionNumber
			AND b.Direction = N'out'
			AND a.PassTime < b.PassTime
			AND a.PostID != b.PostID
	)
GROUP BY a.CarNumber, a.CarRegionNumber, RegionNames.Name
GO

-- Показать иногородние автомобили
SELECT a.CarNumber + convert(nvarchar(3), a.CarRegionNumber) as [Номер и код региона], RegionNames.Name as [Регион]
FROM Passes a, RegionNames, RegionNumbers
WHERE a.Direction = N'in'
	AND EXISTS(
		SELECT TOP 1 * FROM Passes b
		WHERE a.CarNumber = b.CarNumber
			AND a.CarRegionNumber = b.CarRegionNumber
			AND b.Direction = N'out'
			AND a.PassTime < b.PassTime
			AND a.PostID = b.PostID
			AND a.CarRegionNumber = RegionNumbers.Number
			AND RegionNumbers.Code = RegionNames.Code
	)
GROUP BY a.CarNumber, a.CarRegionNumber, RegionNames.Name
GO


-- Показать остальные автомобили
SELECT z.CarNumber + convert(nvarchar(3), z.CarRegionNumber) as [Номер и код региона], x.Name as [Регион]
	FROM Passes z, RegionNames x, RegionNumbers v
	WHERE z.CarNumber NOT IN (
		SELECT a.CarNumber
			FROM Passes a, RegionNames b, RegionNumbers c
			WHERE a.CarRegionNumber = c.Number
				AND c.Code = b.Code
				AND b.Name = N'Свердловская обл.'
				AND a.Direction = N'out'
				AND EXISTS(
					SELECT TOP 1 * FROM Passes b
					WHERE a.PassTime < b.PassTime
						AND a.CarNumber = b.CarNumber
						AND a.CarRegionNumber = b.CarRegionNumber
						AND b.Direction = N'in'
				)
			GROUP BY a.CarNumber
	)
	AND z.CarNumber NOT IN (
		SELECT a.CarNumber
			FROM Passes a, RegionNames b, RegionNumbers e
			WHERE b.Name != N'Свердловская обл.'
				AND e.Code = b.Code
				AND a.CarRegionNumber = e.Number
				AND a.Direction = N'in'
				AND EXISTS(
					SELECT TOP 1 * FROM Passes b
					WHERE a.CarNumber = b.CarNumber
						AND a.CarRegionNumber = b.CarRegionNumber
						AND b.Direction = N'out'
						AND a.PassTime < b.PassTime
						AND a.PostID != b.PostID
				)
			GROUP BY a.CarNumber
	)
	AND z.CarNumber NOT IN (
		SELECT a.CarNumber
		FROM Passes a, RegionNames e, RegionNumbers d
		WHERE a.Direction = N'in'
			AND EXISTS(
				SELECT TOP 1 * FROM Passes b
				WHERE a.CarNumber = b.CarNumber
					AND a.CarRegionNumber = b.CarRegionNumber
					AND b.Direction = N'out'
					AND a.PassTime < b.PassTime
					AND a.PostID = b.PostID
					AND a.CarRegionNumber = d.Number
					AND d.Code = e.Code
			)
		GROUP BY a.CarNumber
	)
	AND z.CarRegionNumber = v.Number
	AND v.Code = x.Code
GROUP BY z.CarNumber, z.CarRegionNumber, x.Name
GO


-- Проверка CHECK'а на добавление некорректного номера региона
INSERT INTO RegionNumbers
VALUES
(999, 69)
GO

-- Проверка CHECK'а на указание некорректного номера автомобиля
INSERT INTO Passes
VALUES
(1, N'Й123РО', 69, '14:12:36', N'out')
GO

-- Проверка триггера на вставку 
INSERT INTO Passes
VALUES
(1, 'М777ММ', 66, '15:24:25', N'in'),
(1, 'М777ММ', 66, '16:24:25', N'in')
GO
INSERT INTO Passes
VALUES
(1, 'М777ММ', 66, '15:24:25', N'out'),
(1, 'М777ММ', 66, '16:24:25', N'out')
GO
