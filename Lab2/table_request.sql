USE master
GO

IF EXISTS (
	SELECT name
		FROM sys.databases
		WHERE name = N'Sheludyakov2'
)
ALTER DATABASE Sheludyakov2 set single_user with rollback immediate
GO

IF EXISTS (
	SELECT name
		FROM sys.databases
		WHERE name = N'Sheludyakov2'
)
DROP DATABASE Sheludyakov2
GO

CREATE DATABASE Sheludyakov2
GO

USE Sheludyakov2
GO

IF EXISTS(
  SELECT *
    FROM sys.schemas
   WHERE name = N'Shell'
)
DROP SCHEMA Shell
GO

CREATE SCHEMA Shell
GO

CREATE TABLE Shell.TransitionInfo
(
	TransitionID smallint IDENTITY(0,1) NOT NULL,
	PostID smallint NULL,
	CarNumber nvarchar(6) NULL,
	CarRegionNumber nvarchar(3) NULL,
	TransitionTime time NULL,
	TransitionDirection nvarchar(5) NULL,
    CONSTRAINT PK_CarNumber PRIMARY KEY (TransitionID),
	CONSTRAINT CHK_CarNumber CHECK
	(
		LEN(CarNumber) = 6
		AND NOT (SUBSTRING(CarNumber, 1, 1) LIKE N'%[^������������]%')
		AND NOT (SUBSTRING(CarNumber, 2, 3) LIKE N'%[^0-9]%')
		AND (SUBSTRING(CarNumber, 2, 3) != N'000')
		AND NOT (SUBSTRING(CarNumber, 5, 2) LIKE N'%[^������������]%')
		AND NOT (CarRegionNumber LIKE N'%[^0-9]%')
		AND (((LEN(CarRegionNumber) = 3) AND (SUBSTRING(CarRegionNumber, 1, 1) LIKE N'[127]')) OR (LEN(CarRegionNumber) = 2))
		AND TransitionDirection IN (N'�����', N'�����')
	)
)
GO

CREATE TABLE Shell.RegionInfo
(
	RegionCode smallint NOT NULL,
	RegionName nvarchar(64) NULL,
    CONSTRAINT PK_RegionCode PRIMARY KEY (RegionCode)
)
GO

CREATE TABLE Shell.CarRegionNumberInfo
(
	CarRegionNumber nvarchar(3) NOT NULL,
	RegionCode smallint NULL,
    CONSTRAINT PK_CarRegionNumber PRIMARY KEY (CarRegionNumber)
)
GO

ALTER TABLE Shell.TransitionInfo ADD
	CONSTRAINT FK_CarRegionNumber FOREIGN KEY (CarRegionNumber)
	REFERENCES Shell.CarRegionNumberInfo(CarRegionNumber)
GO

ALTER TABLE Shell.CarRegionNumberInfo ADD
	CONSTRAINT FK_RegionCode FOREIGN KEY (RegionCode)
	REFERENCES Shell.RegionInfo(RegionCode)
GO

CREATE TRIGGER Shell.TransitionTrigger
	ON Shell.TransitionInfo
	AFTER INSERT
AS
BEGIN
	IF EXISTS (
		SELECT *
		FROM inserted, TransitionInfo
		WHERE inserted.CarNumber = TransitionInfo.CarNumber
			AND inserted.CarRegionNumber = TransitionInfo.CarRegionNumber
			AND TransitionInfo.TransitionTime = (
				SELECT max(TransitionInfo.TransitionTime)
				FROM TransitionInfo
				WHERE inserted.TransitionID != TransitionInfo.TransitionID
					AND inserted.TransitionTime > TransitionInfo.TransitionTime
			)
			AND inserted.TransitionDirection = TransitionInfo.TransitionDirection
	)
	BEGIN
        PRINT '�������� ������'
        ROLLBACK TRANSACTION
    END
    ELSE
	BEGIN
		PRINT '������ ��������� ������'
	END
END
GO

INSERT INTO Shell.RegionInfo
VALUES
(1, N'���������� ������'),
(2, N'���������� ������������'),
(3, N'���������� �������'),
(4, N'���������� �����'),
(5, N'���������� ��������'),
(6, N'���������� ���������'),
(7, N'���������-���������� ����������'),
(8, N'���������� ��������'),
(9, N'���������-���������� ����������'),
(10, N'���������� �������'),
(11, N'���������� ����'),
(12, N'���������� ����� ��'),
(13, N'���������� ��������'),
(14, N'���������� ���� (������)'),
(15, N'���������� �������� ������'),
(16, N'���������� ���������'),
(17, N'���������� ����'),
(18, N'���������� ����������'),
(19, N'���������� �������'),
(20, N'��������� ����������'),
(21, N'��������� ����������'),
(22, N'��������� ����'),
(23, N'������������� ����'),
(24, N'������������ ����'),
(25, N'���������� ����'),
(26, N'�������������� ����'),
(27, N'����������� ����'),
(28, N'�������� �������'),
(29, N'������������� �������'),
(30, N'������������ �������'),
(31, N'������������ �������'),
(32, N'�������� �������'),
(33, N'������������ �������'),
(34, N'������������� �������'),
(35, N'����������� �������'),
(36, N'����������� �������'),
(37, N'���������� �������'),
(38, N'��������� �������'),
(39, N'��������������� �������'),
(40, N'��������� �������'),
(41, N'���������� ����'),
(42, N'����������� �������'),
(43, N'��������� �������'),
(44, N'����������� �������'),
(45, N'���������� �������'),
(46, N'������� �������'),
(47, N'������������� �������'),
(48, N'�������� �������'),
(49, N'����������� �������'),
(50, N'���������� �������'),
(51, N'���������� �������'),
(52, N'������������� �������'),
(53, N'������������ �������'),
(54, N'������������� �������'),
(55, N'������ �������'),
(56, N'������������ �������'),
(57, N'��������� �������'),
(58, N'���������� �������'),
(59, N'�������� ����'),
(60, N'��������� �������'),
(61, N'���������� �������'),
(62, N'��������� �������'),
(63, N'��������� �������'),
(64, N'����������� �������'),
(65, N'����������� �������'),
(66, N'������������ �������'),
(67, N'���������� �������'),
(68, N'���������� �������'),
(69, N'�������� �������'),
(70, N'������� �������'),
(71, N'�������� �������'),
(72, N'��������� �������'),
(73, N'����������� �������'),
(74, N'����������� �������'),
(75, N'������������� ����'),
(76, N'����������� �������'),
(77, N'�. ������'),
(78, N'�. �����-���������'),
(79, N'��������� ���������� �������'),
(83, N'�������� ���������� �����'),
(86, N'�����-���������� ���������� ����� - ����'),
(87, N'��������� ���������� �����'),
(89, N'�����-�������� ���������� �����'),
(91, N'���������� ����'),
(92, N'�����������')
GO

INSERT INTO Shell.CarRegionNumberInfo
VALUES
(N'01', 1),
(N'02', 2),
(N'102', 2),
(N'03', 3),
(N'04', 4),
(N'05', 5),
(N'06', 6),
(N'07', 7),
(N'08', 8),
(N'09', 9),
(N'10', 10),
(N'11', 11),
(N'12', 12),
(N'13', 13),
(N'113', 13),
(N'14', 14),
(N'15', 15),
(N'16', 16),
(N'116', 16),
(N'716', 16),
(N'17', 17),
(N'18', 18),
(N'19', 19),
(N'20', 20),
(N'21', 21),
(N'22', 22),
(N'23', 23),
(N'24', 24),
(N'25', 25),
(N'26', 26),
(N'27', 27),
(N'28', 28),
(N'29', 29),
(N'30', 30),
(N'31', 31),
(N'32', 32),
(N'33', 33),
(N'34', 34),
(N'35', 35),
(N'36', 36),
(N'37', 37),
(N'38', 38),
(N'39', 39),
(N'40', 40),
(N'41', 41),
(N'42', 42),
(N'43', 43),
(N'44', 44),
(N'45', 45),
(N'46', 46),
(N'47', 47),
(N'48', 48),
(N'49', 49),
(N'50', 50),
(N'51', 51),
(N'52', 52),
(N'53', 53),
(N'54', 54),
(N'55', 55),
(N'56', 56),
(N'57', 57),
(N'58', 58),
(N'59', 59),
(N'60', 60),
(N'61', 61),
(N'62', 62),
(N'63', 63),
(N'64', 64),
(N'65', 65),
(N'66', 66),
(N'96', 66),
(N'196', 66),
(N'67', 67),
(N'68', 68),
(N'69', 69),
(N'70', 70),
(N'71', 71),
(N'72', 72),
(N'73', 73),
(N'74', 74),
(N'174', 74),
(N'75', 75),
(N'76', 76),
(N'77', 77),
(N'97', 77),
(N'99', 77),
(N'177', 77),
(N'197', 77),
(N'199', 77),
(N'777', 77),
(N'799', 77),
(N'78', 78),
(N'98', 78),
(N'178', 78),
(N'79', 79),
(N'83', 83),
(N'86', 86),
(N'87', 87),
(N'89', 89),
(N'91', 91),
(N'92', 92)
GO

/* valid data */
INSERT INTO Shell.TransitionInfo
VALUES
(0, N'�649��', '96', '00:01:10', N'�����'), 
(0, N'�649��', '96', '00:01:11', N'�����'), 
(3, N'�637��', '96', '00:01:13', N'�����'), 
(3, N'�637��', '96', '00:01:14', N'�����'), 
(0, N'�357��', '196', '00:01:11', N'�����'), 
(0, N'�649��', '96', '00:01:12', N'�����'), 
(0, N'�649��', '96', '00:01:13', N'�����'), 
(0, N'�357��', '196', '00:01:12', N'�����'), 
(0, N'�357��', '196', '00:01:13', N'�����'), 
(0, N'�100��', '174', '00:01:16', N'�����'),
(0, N'�100��', '174', '00:01:17', N'�����'),
(0, N'�121��', '174', '00:01:17', N'�����'),
(1, N'�121��', '174', '00:01:18', N'�����'),
(0, N'�135��', '75', '00:01:17', N'�����'),
(1, N'�135��', '75', '00:01:18', N'�����'),
(2, N'�121��', '25', '03:01:18', N'�����'),
(2, N'�121��', '25', '03:01:18', N'�����'),
(0, N'�345��', '34', '00:01:10', N'�����'),
(6, N'�649��', '96', '00:01:11', N'�����'),
(0, N'�357��', '25', '00:01:11', N'�����'),
(0, N'�649��', '96', '00:01:12', N'�����'),
(0, N'�649��', '96', '00:01:13', N'�����'),
(0, N'�532��', '37', '00:01:17', N'�����'),
(0, N'�649��', '96', '00:01:12', N'�����'),
(0, N'�649��', '96', '00:01:13', N'�����'),
(0, N'�456��', '42', '00:01:17', N'�����'),
(0, N'�357��', '25', '00:01:12', N'�����'),
(4, N'�456��', '42', '00:01:18', N'�����'),
(0, N'�357��', '25', '00:01:13', N'�����'),
(0, N'�100��', '174', '00:01:16', N'�����'),
(0, N'�100��', '174', '00:01:17', N'�����'),
(0, N'�522��', '37', '00:01:17', N'�����'),
(1, N'�522��', '37', '00:01:18', N'�����'),
(2, N'�121��', '25', '03:01:18', N'�����'),
(2, N'�156��', '70', '03:01:18', N'�����'),
(0, N'�357��', '25', '00:01:13', N'�����'),
(0, N'�100��', '174', '00:01:16', N'�����'),
(0, N'�100��', '174', '00:01:17', N'�����'),
(0, N'�533��', '72', '00:01:17', N'�����'),
(1, N'�522��', '37', '00:01:18', N'�����'),
(2, N'�344��', '25', '03:01:18', N'�����'),
(2, N'�121��', '25', '03:01:18', N'�����'),

(2, N'�121��', '77', '11:09:00', N'�����'),
(2, N'�121��', '77', '12:15:20', N'�����'),
(3, N'�825��', '63', '01:05:23', N'�����'),
(3, N'�825��', '63', '13:05:23', N'�����')
GO


DECLARE	@region nvarchar(64)
SET @region = N'������������ �������'


-- �������
SELECT a.CarNumber + a.CarRegionNumber AS [����� ����������],
    RegionInfo.RegionName AS [������]
FROM Shell.TransitionInfo a, Shell.RegionInfo, Shell.CarRegionNumberInfo
WHERE a.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
	AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode
	AND RegionInfo.RegionName = @region
	AND a.TransitionDirection = N'�����'
    AND EXISTS (
		SELECT TOP 1 *
		FROM Shell.TransitionInfo b
		WHERE a.CarNumber = b.CarNumber
			AND a.CarRegionNumber = b.CarRegionNumber
			AND b.TransitionDirection = N'�����'
			AND a.TransitionTime < b.TransitionTime
	) 


-- ����������
SELECT a.CarNumber + a.CarRegionNumber AS [����� ����������],
    RegionInfo.RegionName AS [������]
FROM Shell.TransitionInfo a, Shell.RegionInfo, Shell.CarRegionNumberInfo
WHERE a.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
	AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode
	AND RegionInfo.RegionName != @region
	AND a.TransitionDirection = N'�����'
    AND EXISTS (
		SELECT TOP 1 *
		FROM Shell.TransitionInfo b
		WHERE a.CarNumber = b.CarNumber
			AND a.CarRegionNumber = b.CarRegionNumber
			AND b.TransitionDirection = N'�����'
			AND a.TransitionID != b.TransitionID
			AND a.TransitionTime < b.TransitionTime
			AND a.PostID != b.PostID
	)


-- �����������
SELECT a.CarNumber + a.CarRegionNumber AS [����� ����������],
    RegionInfo.RegionName AS [������]
FROM Shell.TransitionInfo a, Shell.RegionInfo, Shell.CarRegionNumberInfo
WHERE a.TransitionDirection = N'�����'
	AND a.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
	AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode
	AND EXISTS(
		SELECT TOP 1 * FROM Shell.TransitionInfo b
		WHERE a.CarNumber = b.CarNumber
			AND a.CarRegionNumber = b.CarRegionNumber
			AND b.TransitionDirection = N'�����'
			AND a.TransitionTime < b.TransitionTime
			AND a.PostID = b.PostID
	)


SELECT r.CarNumber + r.CarRegionNumber AS [����� ����������],
    RegionInfo.RegionName AS [������]
FROM Shell.TransitionInfo r, Shell.RegionInfo, Shell.CarRegionNumberInfo
WHERE r.CarNumber NOT IN (
	SELECT a.CarNumber
	FROM Shell.TransitionInfo a, Shell.RegionInfo, Shell.CarRegionNumberInfo
	WHERE a.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
		AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode
		AND RegionInfo.RegionName = @region
		AND a.TransitionDirection = N'�����'
		AND EXISTS (
			SELECT TOP 1 *
			FROM Shell.TransitionInfo b
			WHERE a.CarNumber = b.CarNumber
				AND a.CarRegionNumber = b.CarRegionNumber
				AND b.TransitionDirection = N'�����'
				AND a.TransitionTime < b.TransitionTime
		)
	)
	AND r.CarNumber NOT IN (
		SELECT a.CarNumber
		FROM Shell.TransitionInfo a, Shell.RegionInfo, Shell.CarRegionNumberInfo
		WHERE a.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
			AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode
			AND RegionInfo.RegionName != @region
			AND a.TransitionDirection = N'�����'
			AND EXISTS (
				SELECT TOP 1 *
				FROM Shell.TransitionInfo b
				WHERE a.CarNumber = b.CarNumber
					AND a.CarRegionNumber = b.CarRegionNumber
					AND b.TransitionDirection = N'�����'
					AND a.TransitionID != b.TransitionID
					AND a.TransitionTime < b.TransitionTime
					AND a.PostID != b.PostID
			)
	)
	AND r.CarNumber NOT IN (
		SELECT a.CarNumber
		FROM Shell.TransitionInfo a, Shell.RegionInfo, Shell.CarRegionNumberInfo
		WHERE a.TransitionDirection = N'�����'
			AND a.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
			AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode
			AND EXISTS(
				SELECT TOP 1 * FROM Shell.TransitionInfo b
				WHERE a.CarNumber = b.CarNumber
					AND a.CarRegionNumber = b.CarRegionNumber
					AND b.TransitionDirection = N'�����'
					AND a.TransitionTime < b.TransitionTime
					AND a.PostID = b.PostID
			)
	)
	AND r.CarRegionNumber = CarRegionNumberInfo.CarRegionNumber
	AND CarRegionNumberInfo.RegionCode = RegionInfo.RegionCode