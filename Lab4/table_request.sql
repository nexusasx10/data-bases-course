USE master
GO

IF EXISTS (
	SELECT name
		FROM sys.databases
		WHERE name = N'Sheludyakov2'
)
ALTER DATABASE Sheludyakov2 set single_user with rollback immediate
GO

IF EXISTS (
	SELECT name
		FROM sys.databases
		WHERE name = N'Sheludyakov2'
)
DROP DATABASE Sheludyakov2
GO

CREATE DATABASE Sheludyakov2
GO

USE Sheludyakov2
GO

IF EXISTS(
  SELECT *
    FROM sys.schemas
   WHERE name = N'Shell'
)
DROP SCHEMA Shell
GO

CREATE SCHEMA Shell
GO

CREATE TABLE Shell.TarifInfo
(
	Name nvarchar(32) NOT NULL,
	SubscriptionFee money NULL,
	MonthMinuteLimit int NULL,
	ExtraMinutePrice money NULL,
    CONSTRAINT PK_Name PRIMARY KEY (Name)
)
GO

CREATE FUNCTION Shell.OptimalTarif (@minuteCount int)
RETURNS nvarchar(32)
BEGIN
	IF @minuteCount > 44640 OR @minuteCount <= 0
	BEGIN
		RETURN NULL
	END
	DECLARE @minNorm nvarchar(32)
	SET @minNorm = (
		SELECT TOP 1 Name
		FROM Shell.TarifInfo
		WHERE MonthMinuteLimit >= @minuteCount
			AND SubscriptionFee = (
				SELECT MIN(SubscriptionFee)
				FROM Shell.TarifInfo
				WHERE MonthMinuteLimit >= @minuteCount
			)
	)
	DECLARE @minOver nvarchar(32)
	SET @minOver = (
		SELECT TOP 1 Name
		FROM Shell.TarifInfo
		WHERE MonthMinuteLimit < @minuteCount
			AND SubscriptionFee + ((@minuteCount - MonthMinuteLimit) * ExtraMinutePrice) = (
				SELECT MIN(SubscriptionFee + ((@minuteCount - MonthMinuteLimit) * ExtraMinutePrice))
				FROM Shell.TarifInfo
				WHERE MonthMinuteLimit < @minuteCount
			)
	)
	DECLARE @result nvarchar(32)
	IF (
		SELECT SubscriptionFee
		FROM Shell.TarifInfo
		WHERE Name = @minNorm
	) > (
		SELECT SubscriptionFee + ((@minuteCount - MonthMinuteLimit) * ExtraMinutePrice)
		FROM Shell.TarifInfo
		WHERE Name = @minOver
	)
		SET @result = @minOver
	ELSE
		SET @result = @minNorm
	RETURN @result
END
GO

CREATE FUNCTION Shell.OptimalTarifs (@minValue int, @maxValue int)
RETURNS @result TABLE (
   MinTime int NULL,
   MaxTime int NULL,
   OptimalTarif nvarchar(32) NULL
) 
AS
BEGIN
	DECLARE @minMinutes int
	DECLARE @minutes int
	DECLARE @prevOptimalTarif nvarchar(32)
	DECLARE @nextOptimalTarif nvarchar(32)
	SET @minutes = @minValue
	WHILE @minutes <= @maxValue
	BEGIN
		SET @nextOptimalTarif = Shell.OptimalTarif(@minutes)
		IF @nextOptimalTarif != @prevOptimalTarif
		BEGIN
			INSERT INTO @result ()
		END
		SET @minutes = @minutes + 1
	END
END
GO

INSERT INTO Shell.TarifInfo
VALUES
(N'Без абонентской платы', 0, 0, 2),
(N'Промежуточный', 150, 300, 5),
(N'Безлимитный', 200, 32767, 0)
GO

PRINT Shell.OptimalTarif(0)
PRINT Shell.OptimalTarif(1)
PRINT Shell.OptimalTarif(74)
PRINT Shell.OptimalTarif(75)
PRINT Shell.OptimalTarif(309)
PRINT Shell.OptimalTarif(310)
PRINT Shell.OptimalTarif(32767)
PRINT Shell.OptimalTarif(2147483647)
