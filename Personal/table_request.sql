USE master
GO

IF EXISTS (
	SELECT name
	FROM sys.databases
	WHERE name = N'Sheludyakov2'
)
ALTER DATABASE Sheludyakov2 set single_user with rollback immediate
GO

IF EXISTS (
	SELECT name
	FROM sys.databases
	WHERE name = N'Sheludyakov2'
)
DROP DATABASE Sheludyakov2
GO

CREATE DATABASE Sheludyakov2
GO

USE Sheludyakov2
GO

IF EXISTS(
    SELECT *
    FROM sys.schemas
    WHERE name = N'Shell'
)
DROP SCHEMA Shell
GO

CREATE SCHEMA Shell
GO

CREATE TABLE Shell.Addresses
(
    AddressId smallint IDENTITY(0,1) NOT NULL,
	PostIndex nvarchar(6),
	City nvarchar(32),
	Street nvarchar(32),
	House nvarchar(16)
	CONSTRAINT PK_AddressId PRIMARY KEY (AddressId)
)
GO

CREATE TABLE Shell.Materials
(
	MaterialId smallint IDENTITY(0,1) NOT NULL,
    ClassCode nvarchar(32),
	GroupCode nvarchar(32),
	Name nvarchar(32)
	CONSTRAINT PK_MaterialId PRIMARY KEY (MaterialId)
)
GO

CREATE TABLE Shell.Units
(
    UnitId smallint IDENTITY(0,1) NOT NULL,
	Unit nvarchar(32),
	MaterialId smallint
	CONSTRAINT PK_UnitId PRIMARY KEY (UnitId),
	CONSTRAINT FK_MaterialIdU FOREIGN KEY (MaterialId)
	REFERENCES Shell.Materials(MaterialId)
)
GO

CREATE TABLE Shell.Providers
(
	ProviderCode smallint IDENTITY(0,1) NOT NULL,
	ITN nchar(12),
	LegalAddressId smallint,
	BankAddressId smallint,
	BankAccountNumber nchar(16)
	CONSTRAINT PK_ProviderCode PRIMARY KEY (ProviderCode),
	CONSTRAINT FK_LegalAddressId FOREIGN KEY (LegalAddressId)
	REFERENCES Shell.Addresses(AddressId),
	CONSTRAINT FK_BankAddressId FOREIGN KEY (BankAddressId)
	REFERENCES Shell.Addresses(AddressId)
)
GO

CREATE TABLE Shell.Shelfs
(
    ShelfCode smallint IDENTITY(0,1) NOT NULL,
	LocationId smallint,
	Capacity smallint
	CONSTRAINT PK_ShelfCode PRIMARY KEY (ShelfCode),
	CONSTRAINT FK_LocationId FOREIGN KEY (LocationId)
	REFERENCES Shell.Addresses(AddressId)
)

CREATE TABLE Shell.StoreUnits
(
	OrderNumber smallint IDENTITY(0,1) NOT NULL,
	ReceiveDate smalldatetime,
	ProviderCode smallint,
	DocumentCode smallint,
	DocumentNumber smallint,
	MaterialId smallint,
	UnitId smallint,
	MaterialCount smallint,
	MaterialCountLeft smallint,
	UnitPrice smallmoney,
	ShelfLife date,
	ShelfCode smallint
	CONSTRAINT PK_OrderNumber PRIMARY KEY (OrderNumber),
	CONSTRAINT FK_MaterialId FOREIGN KEY (MaterialId)
	REFERENCES Shell.Materials(MaterialId),
	CONSTRAINT FK_ProviderCode FOREIGN KEY (ProviderCode)
	REFERENCES Shell.Providers(ProviderCode),
	CONSTRAINT FK_ShelfCode FOREIGN KEY (ShelfCode)
	REFERENCES Shell.Shelfs(ShelfCode),
	CONSTRAINT FK_UnitIdS FOREIGN KEY (UnitId)
	REFERENCES Shell.Units(UnitId)
)
GO

CREATE TABLE Shell.Issuings
(
    MaterialId smallint,
	UnitId smallint,
	MaterialCount smallint,
	ResponsibleName nvarchar(64),
	IssuingDate smalldatetime,
    TargetName nvarchar(64)
	CONSTRAINT FK_MaterialIdI FOREIGN KEY (MaterialId)
	REFERENCES Shell.Materials(MaterialId),
	CONSTRAINT FK_UnitIdI FOREIGN KEY (UnitId)
	REFERENCES Shell.Units(UnitId)
)
GO

CREATE TRIGGER Shell.LeftTrigger
	ON Shell.Issuings
	AFTER INSERT
AS
BEGIN
	IF EXISTS(
	    SELECT *
		FROM inserted, Shell.StoreUnits
		WHERE inserted.MaterialCount > StoreUnits.MaterialCount
		    AND inserted.MaterialId = StoreUnits.MaterialId
	)
	BEGIN
        PRINT '������������ ��������'
        ROLLBACK TRANSACTION
    END
    ELSE
	BEGIN
		UPDATE Shell.StoreUnits
		SET MaterialCountLeft = (
		    SELECT StoreUnits.MaterialCount - inserted.MaterialCount
			FROM inserted, Shell.StoreUnits
			WHERE inserted.MaterialId = StoreUnits.MaterialId
		)
	END
END
GO

INSERT INTO Shell.Addresses
VALUES
(N'412366', N'������������', N'������', N'59�'),
(N'436796', N'������������', N'������', N'60'),
(N'342446', N'������������', N'�����������', N'32'),
(N'436796', N'������������', N'������', N'60'),
(N'236152', N'������', N'����������', N'34'),
(N'236152', N'������', N'��������', N'2'),
(N'235552', N'������', N'����� ������', N'53'),
(N'266152', N'������', N'���������', N'32'),
(N'233352', N'������', N'����������', N'74'),
(N'226152', N'������', N'����������', N'2'),
(N'236352', N'������', N'����������', N'34'),
(N'345603', N'�����', N'��������������', N'4�'),
(N'844526', N'���������', N'������', N'35'),
(N'831479', N'���������', N'������', N'12'),
(N'844526', N'���������', N'������', N'36'),
(N'831569', N'���������', N'������', N'14'),
(N'172684', N'�����-���������', N'�����������', N'2'),
(N'670083', N'�����-���������', N'��������', N'23'),
(N'675483', N'�����-���������', N'�������', N'7'),
(N'347003', N'�����-���������', N'��������', N'6')
GO

INSERT INTO Shell.Materials
VALUES
(N'��������������', N'���������', N'�����'),
(N'��������������', N'���������', N'����'),
(N'��������������', N'������������ �����', N'������'),
(N'��������������', N'������������ �����', N'������������ ����'),
(N'�������� �������', N'������������� ��������', N'������'),
(N'�������� �������', N'������������� ��������', N'�����'),
(N'�������� �������', N'������������� ��������', N'���'),
(N'�������� �������', N'�����', N'��������� �����'),
(N'�������� �������', N'�����', N'���'),
(N'���', N'�������', N'��������� �������')
GO

INSERT INTO Shell.Units
VALUES
(N'�����', 0),
(N'����', 1),
(N'����', 2),
(N'����', 3),
(N'����', 4),
(N'����', 5),
(N'���������', 6),
(N'���������', 7),
(N'���������', 8),
(N'����', 9)
GO

INSERT INTO Shell.Providers
VALUES
(N'763987474284', 0, 2, N'7362645284626601'),
(N'820019371254', 1, 2, N'2786806901678950'),
(N'364563546368', 3, 4, N'7362545284626601'),
(N'937527637273', 3, 4, N'2777474901678950'),
(N'376278835636', 5, 7, N'7362654684626601'),
(N'738346736373', 6, 7, N'2786806701678950'),
(N'931958162579', 8, 8, N'7362352572976601'),
(N'798165978164', 6, 9, N'278680690878950'),
(N'117363677565', 8, 2, N'7362353455626601'),
(N'236465426235', 11, 10, N'2786345901678950'),
(N'262562654254', 12, 13, N'7362655534526601'),
(N'820235612554', 18, 13, N'2262552701678950')
GO

INSERT INTO Shell.Shelfs
VALUES
(9, 130),
(13, 1344),
(5, 1000),
(17, 2000),
(14, 4221),
(16, 100),
(17, 100),
(18, 100),
(9, 100),
(10, 1000),
(7, 512),
(12, 360),
(19, 7000),
(9, 200),
(16, 1450)
GO

INSERT INTO Shell.StoreUnits
VALUES
('12.12.2017', 0, 2, 314, 0, 0, 100, 100, 20, '12.05.2019', 0),
('05.06.2017', 1, 3, 714, 3, 3, 400, 400, 30, '12.05.2019', 1),
('12.09.2017', 2, 4, 435, 5, 5, 350, 350, 100, '11.05.2019', 2),
('12.10.2017', 3, 5, 519, 1, 1, 70, 70, 10, '01.08.2019', 3),
('07.01.2017', 4, 6, 903, 9, 9, 240, 240, 14, '07.05.2019', 4),
GO

INSERT INTO Shell.StoreUnits
VALUES
(0, 0, 10, N'�����������', '12.05.2019', N'��� ��������'),
(3, 3, 40, N'�����������', '12.05.2019', N'�� ������'),
(5, 5, 350, N'�����������', '11.05.2019', N'��������'),
(1, 1, 170, N'������', '01.08.2019', N'��� �����'),
(7, 7, 40, N'�������', '07.05.2019', N'�� ������'),
GO

SELECT ITN, PostIndex, City, Street, House, ClassCode, GroupCode, Name
FROM Shell.Providers, Shell.Addresses, Shell.StoreUnits, Shell.Materials
WHERE Providers.LegalAddressId = Addresses.AddressId
    AND Addresses.City = N'������������'
	AND StoreUnits.ProviderCode = Providers.ProviderCode
	AND Materials.MaterialId = StoreUnits.MaterialId

SELECT ClassCode, GroupCode, Name
FROM Shell.Materials, Shell.StoreUnits
WHERE StoreUnits.ShelfLife = CONVERT(date, GETDATE())
    AND StoreUnits.MaterialId = Materials.MaterialId

SELECT ClassCode, GroupCode, Name, MaterialCount, UnitPrice * MaterialCount
FROM Shell.Materials, Shell.StoreUnits
WHERE CAST('2014-05-02' AS date) < StoreUnits.ReceiveDate
    AND StoreUnits.ReceiveDate < CAST('2018-05-02' AS date)
	AND Materials.MaterialId = StoreUnits.MaterialId

SELECT TOP 1 ClassCode, GroupCode, Name
FROM Shell.Materials, Shell.StoreUnits
WHERE Materials.MaterialId = StoreUnits.MaterialId
ORDER BY MaterialCount

SELECT ClassCode, GroupCode, Name
FROM Shell.Materials, Shell.StoreUnits
WHERE Materials.MaterialId = StoreUnits.MaterialId
    AND StoreUnits.UnitPrice > (
	    SELECT AVG(UnitPrice)
		FROM Shell.StoreUnits
	)
